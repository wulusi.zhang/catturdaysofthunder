﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatArmController : MonoBehaviour
{
    [SerializeField]
    private Transform startLocation;
    public Transform targetLocation;
    public GameObject Cursor;

    public List<Transform> ArmLocations = new List<Transform>();

    [SerializeField]
    private Transform parent;

    [SerializeField]
    private float armMoveSpeed, armIndex, cursorSpd;
    public bool isRight, isHit, isArcadeControls;

    [SerializeField]
    private GameObject particles, airpoof;

    [SerializeField]
    private GameObject PawUp, PawDown;

    /// <summary>
    /// Player one controls: A, S || Player two controls: K,L
    /// </summary>
    [SerializeField]
    private KeyCode[] controls = new KeyCode[] { KeyCode.LeftControl, KeyCode.S, KeyCode.K, KeyCode.A };

    // Start is called before the first frame update
    void Start()
    {
        startLocation.position = this.transform.position;
        isHit = false;
        isArcadeControls = GameHub.GameManager.isArcadeBuild;
        //DecoupleFromParent();
    }

    // Update is called once per frame
    void Update()
    {
        DetectMovement();
        DistanceCheck();
        SwitchTargets();
        UICursor();
        IncrementScore();
        DetectReset();
    }

    private void DetectReset()
    {
        if (Input.GetKey(KeyCode.Alpha3))
        {
            GameHub.GameManager.ReloadScene();
        }
    }

    private void DetectMovement()
    {
        if (isArcadeControls)
        {

            if (!isRight)
            {
                if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.LeftAlt) ||
                    Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.LeftShift) ||
                        Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.X) || Input.GetKey(KeyCode.C)
                        || Input.GetKey(KeyCode.Alpha5))
                {
                    MoveArm();
                }
                else
                {
                    MoveBack();
                }
            }
            else
            {
                if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) ||
                    Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.W) ||
                        Input.GetKey(KeyCode.E) || Input.GetKey(KeyCode.LeftBracket) || Input.GetKey(KeyCode.RightBracket)
                        || Input.GetKey(KeyCode.Alpha6))
                {
                    MoveArm();
                }
                else
                {
                    MoveBack();
                }
            }
        }
        else
        {
            if (!isRight)
            {
                if (Input.GetKey(KeyCode.A))
                {
                    MoveArm();
                }
                else
                {
                    MoveBack();
                }
            }
            else
            {
                if (Input.GetKey(KeyCode.L))
                {
                    MoveArm();
                }
                else
                {
                    MoveBack();
                }
            }
        }
    }

    private void MoveBack()
    {
        transform.position = Vector3.MoveTowards(transform.position, startLocation.position, armMoveSpeed * Time.deltaTime);

        if (Vector3.Distance(transform.position, startLocation.position) < 0.1f)
        {
            isHit = false;
        }

        PawUp.SetActive(true);
        PawDown.SetActive(false);
    }

    private void MoveArm()
    {
        transform.position = Vector3.MoveTowards(transform.position, targetLocation.position, armMoveSpeed * Time.deltaTime);

        PawUp.SetActive(false);
        PawDown.SetActive(true);
    }

    private void DistanceCheck()
    {
        if (Vector3.Distance(transform.position, targetLocation.position) < 0.1f)
        {
            StartCoroutine(GameHub.GameManager.DelaySound(0, 4, 0, true, true));

            RaycastHit2D HitInfo;
            HitInfo = Physics2D.BoxCast(this.transform.position, new Vector2(2, 1), 0, transform.position, 0f);

            if (HitInfo && !isHit)
            {
                isHit = true;
                //Check if item hit correct to the one that is needed
                Debug.Log("I hit :" + HitInfo.collider.name);
                if (HitInfo.collider.tag != "Heart")
                {
                    HitInfo.collider.gameObject.SetActive(false);
                }
                else
                {
                    if (HitInfo.collider.gameObject.GetComponent<HeartFish>())
                    {
                        HitInfo.collider.gameObject.GetComponent<HeartFish>().fishHealth--;
                    }
                    else
                    {
                        HitInfo.collider.gameObject.GetComponent<GoldFood>().fishHealth--;
                    }
                }
                GameHub.GameManager.CheckFoodItem(HitInfo.collider.gameObject, isRight, this.gameObject);
            }
        }
    }

    public void UpdateTargetLocation()
    {
        armIndex = Mathf.Clamp(armIndex, 0, 2);

        targetLocation = ArmLocations[(int)armIndex];
    }

    public void UICursor()
    {
        Cursor.gameObject.GetComponent<Transform>().position =
            Vector3.MoveTowards(Cursor.gameObject.GetComponent<Transform>().position, targetLocation.position, Time.deltaTime * cursorSpd);
    }

    public void SwitchTargets()
    {
        if (isArcadeControls)
        {
            if (!isRight)
            {
                if (Input.GetKeyUp(KeyCode.UpArrow))
                {
                    armIndex--;
                    UpdateTargetLocation();
                }

                if (Input.GetKeyUp(KeyCode.DownArrow))
                {
                    armIndex++;
                    UpdateTargetLocation();
                }
            }
            else
            {
                if (Input.GetKeyUp(KeyCode.R))
                {
                    armIndex--;
                    UpdateTargetLocation();
                }

                if (Input.GetKeyUp(KeyCode.F))
                {
                    armIndex++;
                    UpdateTargetLocation();
                }
            }
        }
        else
        {
            if (!isRight)
            {
                if (Input.GetKeyUp(KeyCode.S))
                {
                    armIndex--;
                    UpdateTargetLocation();
                }

                if (Input.GetKeyUp(KeyCode.X))
                {
                    armIndex++;
                    UpdateTargetLocation();
                }
            }
            else
            {
                if (Input.GetKeyUp(KeyCode.K))
                {
                    armIndex--;
                    UpdateTargetLocation();
                }

                if (Input.GetKeyUp(KeyCode.M))
                {
                    armIndex++;
                    UpdateTargetLocation();
                }
            }
        }

        armIndex = Mathf.Clamp(armIndex, 0, 2);
    }

    private void IncrementScore()
    {
        if (Input.GetKey(KeyCode.Y))
        {
            GameHub.GameManager.comboScore++;
            GameHub.GameManager.AllScore++;
        }

        if (Input.GetKey(KeyCode.U))
        {
            GameHub.GameManager.comboScore--;
            GameHub.GameManager.AllScore--;
        }
    }
}
