﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLerp : MonoBehaviour
{
    [SerializeField]
    private Transform start, end;

    public float lerpSpd;

    public bool started;

    [SerializeField]
    private GameObject startMenu;

    private void OnEnable()
    {
        Time.timeScale = 0;
        started = false;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (started)
        {
            LerpCamera();
        }
    }

    public void LerpCamera()
    {
        startMenu.SetActive(false);
        this.transform.position = Vector3.Lerp(this.transform.position, end.position, Time.deltaTime * 0.5f);
        Time.timeScale = 1;
    }

    public void StartGame()
    {
        started = true;
    }
}
