﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class sObj_ItemSpawnerParams : ScriptableObject
{
    [Range(1,30)]
    public float speed;

    [Range(-1, 1)]
    public float direction;

    [Range(0.01f, 10)]
    public float spawnCoolDown;
}
