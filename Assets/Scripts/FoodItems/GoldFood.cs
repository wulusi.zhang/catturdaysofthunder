﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldFood : MonoBehaviour
{
    public float fishHealth;

    private void Start()
    {
        StartCoroutine(CheckHealth());
    }
    private IEnumerator CheckHealth()
    {
        while (true)
        {
            CheckHit();
            yield return null;
        }
    }

    private void CheckHit()
    {
        if (fishHealth <= 0)
        {
            GameHub.GameManager.AllScore = 2 * GameHub.GameManager.AllScore;
            this.gameObject.SetActive(false);
        }
    }
}
