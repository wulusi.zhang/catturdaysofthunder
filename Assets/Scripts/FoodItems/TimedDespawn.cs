﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedDespawn : MonoBehaviour
{
    [SerializeField]
    private float despawnTimer;

    public bool isObstruct;

    private void OnEnable()
    {
        StartCoroutine(Countdown(despawnTimer));
    }

    private IEnumerator Countdown(float duration)
    {
        float totalTime = 0;
        while (totalTime <= duration)
        {
            totalTime += Time.deltaTime;
            yield return null;
        }
        this.gameObject.SetActive(false);

        if (isObstruct)
        {       
            this.gameObject.GetComponent<SpriteRenderer>().enabled = true;
            GameHub.GameManager.ob = true;
        }
    }
}
