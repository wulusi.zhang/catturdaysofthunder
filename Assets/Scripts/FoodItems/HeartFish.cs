﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartFish : MonoBehaviour
{

    public float fishHealth, maxHealth;

    private void OnEnable()
    {
        fishHealth = maxHealth;
        StartCoroutine(CheckHealth());
    }

    private IEnumerator CheckHealth()
    {
        while (true)
        {
            HealthCheck();
            yield return null;
        }
    }

    private void HealthCheck()
    {
        if(fishHealth <= 0)
        {
            GameHub.GameManager.Life++;
            this.gameObject.SetActive(false);
        }
    }
}
