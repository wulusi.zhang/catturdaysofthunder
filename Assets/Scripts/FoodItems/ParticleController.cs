﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleController : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem particles;

    private void OnEnable()
    {
        particles.Play();
    }

    private void OnDisable()
    {
        particles.Stop();
    }
}
