﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemMover : MonoBehaviour, ObjectInterface
{
    public float itemSpeed;

    public float direction;

    public void OnObjectSpawn()
    {
        itemSpeed = 0;
        direction = 0;
        StartCoroutine(moveItem());
    }

    private IEnumerator moveItem()
    {
        while (true)
        {
            move();
            yield return null;
        }
    }

    void move()
    {
        transform.localPosition += new Vector3(-direction, 0, 0) * itemSpeed * Time.deltaTime;
    }
}
