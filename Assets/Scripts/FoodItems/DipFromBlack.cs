﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DipFromBlack : MonoBehaviour
{
    public Image blackScreen;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(dip());
    }

    private IEnumerator dip()
    {
        while (true)
        {

            Transition();
            yield return null;

        }
    }

    private void Transition()
    {
        blackScreen.color = new Vector4(0, 0, 0, 1 - (Time.time * 0.15f));
    }
}
