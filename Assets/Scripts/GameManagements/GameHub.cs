﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHub : MonoBehaviour
{
    //Multiton structure granting access to major game system via connected hub
    private static  PoolManager poolManager;

    public static PoolManager PoolManager => ObjectRefUtility.FindReferenceIfNull(ref poolManager);

    private static GameManager gameManager;

    public static GameManager GameManager => ObjectRefUtility.FindReferenceIfNull(ref gameManager);
}
