﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioComponent : MonoBehaviour
{
    private AudioSource audioComp;
    public List<AudioClip> sounds;

    private void Start()
    {
        audioComp = GetComponent<AudioSource>();
    }

    public void PlayRandomClip()
    {
        //audioComp.Stop();
        int randIndex = Random.Range(0, sounds.Count);
        audioComp.clip = sounds[randIndex];
        audioComp.Play();
    }

    public void PlayThisClip(int index)
    {
        if (sounds.Count > index)
        {
            //audioComp.Stop();
            audioComp.clip = sounds[index];
            audioComp.Play();
        }
    }

    public void PlayOneShot(int index)
    {
        if (sounds.Count > index)
        {
            //audioComp.Stop();
            audioComp.clip = sounds[index];
            audioComp.PlayOneShot(sounds[index]);
        }
    }

    public void StopThisClip()
    {
        audioComp.Stop();
    }
}
