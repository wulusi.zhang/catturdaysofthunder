﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private Transform currentGoalListL, currentGoalListR;

    public GameObject LeftGoal, RightGoal;
    public List<GameObject> GoalList = new List<GameObject>();
    public List<GameObject> HardGoalList = new List<GameObject>();

    [SerializeField]
    private List<GameObject> CatExpressions = new List<GameObject>();

    [SerializeField]
    private List<GameObject> CatAudience = new List<GameObject>();

    [SerializeField]
    private List<GameObject> CatCrowd = new List<GameObject>();

    [SerializeField]
    private List<int> ScoreStages = new List<int>();

    [SerializeField]
    private List<int> ComboStages = new List<int>();

    public float AllScore = 0f, comboScore = 0f;

    public float Life, maxLife, catExpressionIndex;

    [SerializeField]
    private GameObject LifeCounter, GameOverPanel;

    [SerializeField]
    private GameObject rightParticle, wrongParticle;

    [SerializeField]
    private GameObject leftArmCtlr, rightArmCtlr;

    [SerializeField]
    private GameObject obstruct1, obstruct2;
    //UI scoring here
    public Text AllScoreUI;

    public bool isArcadeBuild, ob = true, isOnFire = false;

    [SerializeField]
    private List<GameObject> Sounds = new List<GameObject>();

    [SerializeField]
    private GameObject Instructions, StartPanel;

    private void Start()
    {
        Life = maxLife;
        AssignGoals();
        StartCoroutine(RunContinously());
        DeactivateAllChildren(currentGoalListL, LeftGoal);
        DeactivateAllChildren(currentGoalListR, RightGoal);
        GameOverPanel.SetActive(false);
        StartCoroutine(DelaySound(2, 5, 0, true, false));
    }

    public IEnumerator DelaySound(int time, int indexOne, int indexTwo, bool isRandom, bool isOneShot)
    {
        yield return new WaitForSeconds(time);

        //Plate Sound, 0 = Ambient, 1 = Success, 2 = Failiure, 3 = General Meows, 4 = PawSounds, 5 = Background
        if (!isRandom)
        {
            if (!isOneShot)
            {
                Sounds[indexOne].GetComponent<AudioComponent>().PlayThisClip(indexTwo);
            }
            else
            {
                Sounds[indexOne].GetComponent<AudioComponent>().PlayOneShot(indexTwo);
            }
        }
        else
        {
            Sounds[indexOne].GetComponent<AudioComponent>().PlayRandomClip();
        }
    }

    private void AssignGoals()
    {
        for (int i = 0; i < GoalList.Count; i++)
        {
            GameObject spawnedGoalLeft =
            GameHub.PoolManager.SpawnFromPool(GoalList[i].gameObject.name, currentGoalListL.transform.position, Quaternion.identity);

            spawnedGoalLeft.transform.localScale = new Vector3(1, 1, 1);
            spawnedGoalLeft.transform.position += new Vector3(0.5f, 0, 0);
            spawnedGoalLeft.GetComponent<TimedDespawn>().enabled = false;

            GameObject spawnedGoalRight =
            GameHub.PoolManager.SpawnFromPool(GoalList[i].gameObject.name, currentGoalListR.transform.position, Quaternion.identity);

            spawnedGoalRight.transform.localScale = new Vector3(1, 1, 1);
            spawnedGoalRight.transform.position += new Vector3(-0.5f, 0, 0);
            spawnedGoalRight.GetComponent<TimedDespawn>().enabled = false;

            spawnedGoalLeft.transform.SetParent(currentGoalListL);
            spawnedGoalRight.transform.SetParent(currentGoalListR);
        }
    }

    public void DeactivateAllChildren(Transform parent, GameObject GoalSide)
    {
        var childCount = parent.childCount;

        for (int i = 0; i < childCount; i++)
        {
            parent.GetChild(i).gameObject.SetActive(false);
        }

        ActivateSingle(parent, childCount, GoalSide);
    }

    public void ActivateSingle(Transform parent, int childcount, GameObject GoalSide)
    {
        var ActivatedGoal = parent.GetChild(Random.Range(0, childcount)).gameObject;
        ActivatedGoal.SetActive(true);

        if (GoalSide == LeftGoal)
        {
            LeftGoal = ActivatedGoal;
            Debug.Log("goal is " + GoalSide);
        }
        else
        {
            RightGoal = ActivatedGoal;
            Debug.Log("goal is " + GoalSide);
        }
    }

    public void CheckFoodItem(GameObject ItemToCheck, bool isRight, GameObject spawnLocation)
    {
      

        if (isRight)
        {
            if (ItemToCheck.name == RightGoal.name || ItemToCheck.tag == "Heart")
            {
                Debug.Log("score right!");
                AllScore++;
                comboScore++;

                //Plate Sound, 0 = Ambient, 1 = Success, 2 = Failiure, 3 = General Meows, 4 = PawSounds, 5 = Background

                //Random Happy Meow
                StartCoroutine(DelaySound(0, 1, 0, true, true));

                //Plates
                //StartCoroutine(DelaySound(1, 0, 3, false, true));

                GameHub.PoolManager.SpawnFromPool(rightParticle.name, spawnLocation.transform.position, Quaternion.identity);
                //Apply happy expression
                StartCoroutine(ApplyTimedExpression(1));
                //Good particle here
                DeactivateAllChildren(currentGoalListR, RightGoal);
            }
            else
            {
                Debug.Log("missed right!");
                //Bad particle here
                GameHub.PoolManager.SpawnFromPool(wrongParticle.name, spawnLocation.transform.position, Quaternion.identity);
                //Apply sad expression
                StartCoroutine(ApplyTimedExpression(2));

                //Ramndom Sad Meow
                StartCoroutine(DelaySound(0, 2, 0, true, true));
                //Deduct Life
                comboScore = 0;
                isOnFire = false;
                Life--;
                AllScore--;
            }
        }
        else
        {
            if (ItemToCheck.name == LeftGoal.name || ItemToCheck.tag == "Heart")
            {
                Debug.Log("score left!");
                AllScore++;
                comboScore++;
                GameHub.PoolManager.SpawnFromPool(rightParticle.name, spawnLocation.transform.position, Quaternion.identity);
                StartCoroutine(ApplyTimedExpression(1));
                DeactivateAllChildren(currentGoalListL, LeftGoal);
            }
            else
            {
                Debug.Log("missed left!");
                GameHub.PoolManager.SpawnFromPool(wrongParticle.name, spawnLocation.transform.position, Quaternion.identity);
                StartCoroutine(ApplyTimedExpression(2));
                comboScore = 0;
                isOnFire = false;
                Life--;
                AllScore--;
            }
        }
    }

    private IEnumerator RunContinously()
    {
        while (true)
        {
            UpdateUI();
            UpdateCatExpressions();
            CheckScoreState();
            HealthCheck();
            yield return null;
        }
    }

    private void HealthCheck()
    {
        if (Life > maxLife)
        {
            Life = maxLife;
        }
    }

    private void UpdateUI()
    {
        AllScoreUI.text = AllScore.ToString();

        if (Life == 3)
        {
            LifeCounter.transform.GetChild(2).gameObject.SetActive(true);
            LifeCounter.transform.GetChild(1).gameObject.SetActive(true);
            LifeCounter.transform.GetChild(0).gameObject.SetActive(true);
        }

        if (Life == 2)
        {
            LifeCounter.transform.GetChild(2).gameObject.SetActive(false);
            LifeCounter.transform.GetChild(1).gameObject.SetActive(true);
            LifeCounter.transform.GetChild(0).gameObject.SetActive(true);
        }

        if (Life == 1)
        {
            LifeCounter.transform.GetChild(1).gameObject.SetActive(false);
            LifeCounter.transform.GetChild(1).gameObject.SetActive(false);
            LifeCounter.transform.GetChild(0).gameObject.SetActive(true);
        }

        if (Life == 0)
        {
            LifeCounter.transform.GetChild(0).gameObject.SetActive(false);
            LifeCounter.transform.GetChild(1).gameObject.SetActive(false);
            LifeCounter.transform.GetChild(1).gameObject.SetActive(false);
            GameOver();
        }
    }

    public void GameOver()
    {
        leftArmCtlr.GetComponent<CatArmController>().enabled = false;
        rightArmCtlr.GetComponent<CatArmController>().enabled = false;
        GameOverPanel.SetActive(true);
        catExpressionIndex = 2f;
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void spawnParticles(GameObject spawnLocation, GameObject particleToSpawn)
    {
        GameHub.PoolManager.SpawnFromPool(particleToSpawn.name, spawnLocation.transform.position, Quaternion.identity);
    }

    private void UpdateCatExpressions()
    {
        if (catExpressionIndex == 0)
        {
            CatExpressions[0].SetActive(true);
            CatExpressions[1].SetActive(false);
            CatExpressions[2].SetActive(false);
            CatExpressions[3].SetActive(false);
        }

        if (catExpressionIndex == 1)
        {
            CatExpressions[0].SetActive(false);
            CatExpressions[1].SetActive(true);
            CatExpressions[2].SetActive(false);
            CatExpressions[3].SetActive(false);
        }

        if (catExpressionIndex == 2)
        {
            CatExpressions[0].SetActive(false);
            CatExpressions[1].SetActive(false);
            CatExpressions[2].SetActive(true);
            CatExpressions[3].SetActive(false);
        }

        if (catExpressionIndex == 3)
        {
            CatExpressions[0].SetActive(false);
            CatExpressions[1].SetActive(false);
            CatExpressions[2].SetActive(false);
            CatExpressions[3].SetActive(true);
        }
    }

    private IEnumerator ApplyTimedExpression(int expIndex)
    {
        catExpressionIndex = expIndex;
        yield return new WaitForSeconds(1f);
        catExpressionIndex = 0;
    }

    private void CheckScoreState()
    {
        for (int i = 0; i < ScoreStages.Count; i++)
        {
            if (AllScore >= ScoreStages[i])
            {
                CatAudience[i].GetComponentInChildren<Animator>().SetBool("AudienceActive", true);
            }
            else
            {
                CatAudience[i].GetComponentInChildren<Animator>().SetBool("AudienceActive", false);
                //CatAudience[i].SetActive(false);
            }
        }

        for (int i = 0; i < ComboStages.Count; i++)
        {
            if (comboScore >= ComboStages[i])
            {
                CatCrowd[i].GetComponentInChildren<Animator>().SetBool("AudienceActive", true);
            }
            else
            {
                CatCrowd[i].GetComponentInChildren<Animator>().SetBool("AudienceActive", false);
                //CatAudience[i].SetActive(false);
            }
        }

        if (comboScore >= ComboStages[ComboStages.Count - 1])
        {
            isOnFire = true;
            catExpressionIndex = 3;
        }
        else
        {
            
        }

        if (AllScore != 0)
        {
            if (AllScore % 6 == 0 && ob)
            {
                ob = false;
                obstruct1.SetActive(true);
            }

            if (AllScore % 10 == 0 && ob)
            {
                ob = false;
                obstruct2.SetActive(true);
            }
        }
    }

    public void ActivateInstructions()
    {
        Instructions.SetActive(true);
    }

    public void DeactivateInstructions()
    {
        Instructions.SetActive(false);
    }
}
