﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcadeStart : MonoBehaviour
{
    [SerializeField]
    GameObject start, instructions;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.DownArrow))
        {
            start.GetComponent<CameraLerp>().StartGame();
        }

        if (Input.GetKey(KeyCode.F))
        {
            instructions.gameObject.SetActive(true);
        }
    }
}
