﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateRenderers : MonoBehaviour
{

    public float timeDelayDuration;
    public List<Renderer> sprites = new List<Renderer>();
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            sprites.Add(transform.GetChild(i).GetComponent<Renderer>());
        }

        for (int i = 0; i < sprites.Count; i++)
        {
            sprites[i].enabled = false;
        }

        StartCoroutine(Countdown(timeDelayDuration));
    }

    private IEnumerator Countdown(float duration)
    {
        float totalTime = 0;
        while (totalTime <= duration)
        {
            totalTime += Time.deltaTime;
            yield return null;
        }
        activateSprites();
    }

    private void activateSprites()
    {
        for (int i = 0; i < sprites.Count; i++)
        {
            sprites[i].enabled = true;
        }
    }
}
