﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeltControllers : MonoBehaviour
{
    [SerializeField]
    private float timeDifficultyVal = 0f;
    private float scoreDifficultyVal = 0f;
    private float betweenTime = 20f, origBetweenTime;
    private float maxTime = 180f;
    private float maxVal = 1f, minVal = 0f;
    [SerializeField]
    private float origSpeed, origDirection, origCoolDown;
    private float maxSpeed = 30f, minSpeed = 10f, speedVariance = 1.5f;
    private float maxDirection = 1f, minDirection = 0.6f, directionVariance = 0.1f;
    private float maxCoolDown = 3f, minCoolDown = 1.0f, coolDownVariance = 1f;
    [SerializeField]
    private sObj_ItemSpawnerParams itemSpawner;
    [SerializeField]
    private DirectionalArrows arrowIndicator;

    private void Start()
    {
        if(origSpeed == 0f)
        {
            origSpeed = minSpeed;
        }
        if(origDirection == 0f)
        {
            origDirection = minDirection * Mathf.Sign(Random.Range(0f, 1f) - 0.5f);
        }
        if(origCoolDown == 0f)
        {
            origCoolDown = maxCoolDown;
        }
        arrowIndicator.SetOriginalDirections(origDirection);
        StartTimer();
    }

    private void StartTimer()
    {
        SetParamToValues(origSpeed, origDirection, origCoolDown);
        StartCoroutine(DifficultyIncrease());
        StartCoroutine(NumberRandomizer());
    }

    private void StopTimer()
    {
        StopCoroutine(DifficultyIncrease());
        StopCoroutine(NumberRandomizer());
    }

    IEnumerator DifficultyIncrease()
    {
        //Waits a certain amount of time, then assigns generated values to the params
        while (true && itemSpawner != null)
        {
            yield return new WaitForSeconds(betweenTime);
            SetParamToValues(GenerateSpeedValues(), GenerateDirectionValues(), GenerateCoolDownValues());
        }
    }

    IEnumerator NumberRandomizer()
    {
        float timeElapsed = 0f;
        while (true)
        {
            timeElapsed += Time.deltaTime;
            timeDifficultyVal = Mathf.Clamp(timeElapsed / maxTime, minVal, maxVal);
            yield return null;
        }
    }

    private float GenerateSpeedValues()
    {
        float tempMediumVal = (timeDifficultyVal + scoreDifficultyVal) * maxSpeed;
        float tempSpeed = Mathf.Clamp(Random.Range(tempMediumVal - speedVariance, Mathf.Clamp(tempMediumVal + speedVariance, minSpeed, maxSpeed)), minSpeed, maxSpeed);
        return tempSpeed;
    }
    private float GenerateDirectionValues()
    {
        float tempMediumVal = (timeDifficultyVal + scoreDifficultyVal) * maxDirection;
        float tempDir = Mathf.Clamp(Random.Range(tempMediumVal - directionVariance, Mathf.Clamp(tempMediumVal + directionVariance, minDirection, maxDirection)), -maxDirection, maxDirection);
        return tempDir * Mathf.Sign(Random.Range(0f, 1f) - 0.5f);
    }
    private float GenerateCoolDownValues()
    {
        float tempMediumVal = Mathf.Lerp(maxCoolDown, minCoolDown, Mathf.Clamp((timeDifficultyVal + scoreDifficultyVal), 0, 1));
        float tempCoolDown = Mathf.Clamp(Random.Range(tempMediumVal - coolDownVariance, Mathf.Clamp(tempMediumVal + coolDownVariance, minCoolDown, maxCoolDown)), minCoolDown, maxCoolDown);
        return tempCoolDown;
    }

    private void SetParamToValues(float newSpeed, float newDir, float newCoolDown)
    {
        itemSpawner.speed = newSpeed;
        itemSpawner.direction = Mathf.Sign(newDir);
        itemSpawner.spawnCoolDown = Mathf.RoundToInt(newCoolDown) - 0.5f;
        if (arrowIndicator != null) arrowIndicator.CheckShift(newDir);
    }

    private void SetOriginalValues()
    {
        origBetweenTime = betweenTime;
    }

    private void ResetValues()
    {
        timeDifficultyVal = 0f;
        betweenTime = origBetweenTime;
    }
}
