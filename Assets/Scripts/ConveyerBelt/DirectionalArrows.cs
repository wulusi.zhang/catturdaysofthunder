﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionalArrows : MonoBehaviour
{
    private int lastDirection;
    [SerializeField]
    private float flashDuration = 2f, flashHold = 0.4f;
    [SerializeField]
    private int flashTimes = 2;
    [SerializeField]
    private float flashSegmentMin = 0.1f, flashSegmentMax = 1f;
    Color defaultColor = new Color(255, 255, 255, 0);

    private SpriteRenderer spriteComp;

    private void Awake()
    {
        spriteComp = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {

        spriteComp.color = defaultColor;
    }

    public void CheckShift(float tempDir)
    {
        int newDir = Mathf.RoundToInt(Mathf.Sign(tempDir));
        if (newDir != lastDirection)
        {
            SwapDirections();
        }
        lastDirection = newDir;
    }

    private void SwapDirections()
    {
        transform.position = new Vector3((transform.position.x) * -1, transform.position.y, transform.localScale.z);
        transform.localScale = new Vector3((transform.localScale.x) * -1, transform.localScale.y, transform.localScale.z);
        StartCoroutine(ArrowFlashing());
    }

    private IEnumerator ArrowFlashing()
    {
        float flashSegmentLength = Mathf.Clamp(flashDuration / (flashTimes * 2), flashSegmentMin, flashSegmentMax);
        for (int i = 0; i < flashTimes; i++)
        {
            float timeToFade = 0f, timeFromFade = 0f;
            while (timeFromFade < flashSegmentLength)
            {
                timeFromFade += Time.deltaTime;
                spriteComp.color = Color.Lerp(defaultColor, Color.white, timeFromFade / flashSegmentLength);
                yield return null;
            }
            spriteComp.color = Color.white;
            yield return new WaitForSeconds(flashHold);
            while (timeToFade < flashSegmentLength)
            {
                timeToFade += Time.deltaTime;
                spriteComp.color = Color.Lerp(Color.white, defaultColor, timeToFade / flashSegmentLength);
                yield return null;
            }
            spriteComp.color = defaultColor;
            yield return new WaitForSeconds(flashHold);
        }
    }

    public void SetOriginalDirections(float origDirection)
    {
        lastDirection = Mathf.RoundToInt(Mathf.Sign(origDirection));
        transform.position = new Vector3(Mathf.Abs(transform.position.x) * -Mathf.Sign(origDirection), transform.position.y, transform.localScale.z);
        transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x) * -Mathf.Sign(origDirection), transform.localScale.y, transform.localScale.z);
        StartCoroutine(ArrowFlashing());
    }
}
