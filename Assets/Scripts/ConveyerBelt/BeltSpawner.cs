﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeltSpawner : MonoBehaviour
{
    [SerializeField]
    private sObj_ItemSpawnerParams sObj_ItemParams;

    [SerializeField]
    private GameObject beltUnitToSpawn;

    [SerializeField]
    private List<GameObject> BeltList = new List<GameObject>();

    [SerializeField]
    private Transform currentSpawn;
    
    [SerializeField]
    private FoodSpawner targetSpawn;

    float beltDuration;

    //// Start is called before the first frame update
    //void Start()
    //{
    //    targetSpawn = GetComponent<FoodSpawner>();
    //    currentSpawn = targetSpawn.currentSpawn;
    //    StartCoroutine(Countdown(beltDuration));
    //}

    //// Update is called once per frame
    //void Update()
    //{
    //    UpdateItemParams();
    //    UpdateSpawns();
    //}

    //private IEnumerator Countdown(float duration)
    //{
    //    float totalTime = 0;
    //    while (totalTime <= duration)
    //    {
    //        totalTime += Time.deltaTime;
    //        yield return null;
    //    }
    //    spawnFromPool(currentSpawn);
    //}

    //private void spawnFromPool(Transform spawnLoc)
    //{
    //    GameObject spawnedItem = GameHub.PoolManager.SpawnFromPool(beltUnitToSpawn.name, spawnLoc.position, Quaternion.identity);

    //    BeltList.Add(spawnedItem);

    //    StartCoroutine(Countdown(beltDuration));
    //}

    //private void UpdateItemParams()
    //{
    //    for (int i = 0; i < BeltList.Count - 1; i++)
    //    {
    //        var currentSpd = sObj_ItemParams.beltSpeed;
    //        var currentDir = Mathf.Sign(sObj_ItemParams.direction);

    //        var lastSpd = BeltList[i].GetComponent<BeltMover>().itemSpeed;
    //        var lastDir = BeltList[i].GetComponent<BeltMover>().direction;

    //        if (lastSpd != currentSpd || lastDir != currentDir)
    //        {
    //            BeltList[i].GetComponent<BeltMover>().itemSpeed = currentSpd;
    //            //Debug.Log("Lerping speed" + spawnedFood[i].GetComponent<ItemMover>().itemSpeed);
    //            BeltList[i].GetComponent<BeltMover>().direction = currentDir;
    //            //Debug.Log("Lerping dir" + spawnedFood[i].GetComponent<ItemMover>().itemSpeed);
    //            beltDuration = sObj_ItemParams.beltCoolDown;
    //        }
    //        else
    //        {
    //            BeltList[i].GetComponent<BeltMover>().itemSpeed = currentSpd;
    //            BeltList[i].GetComponent<BeltMover>().direction = Mathf.Sign(sObj_ItemParams.direction) * 1f;
    //            beltDuration = sObj_ItemParams.beltCoolDown;
    //        }
    //    }
    //}

    //private void UpdateSpawns()
    //{
    //    currentSpawn = targetSpawn.currentSpawn;
    //}
}
