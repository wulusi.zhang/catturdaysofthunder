﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeltMover : MonoBehaviour
{
    public float itemSpeed;

    public float direction;

    public void OnObjectSpawn()
    {
        //itemSpeed = 0;
        //direction = 0;
        StartCoroutine(moveItem());
    }

    public void Start()
    {
        StartCoroutine(moveItem());
    }

    private IEnumerator moveItem()
    {
        while (true)
        {
            move();
            yield return null;
        }
    }

    void move()
    {
        transform.localPosition += new Vector3(-direction, 0, 0) * itemSpeed * Time.deltaTime;
    }
}
