﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Despawner : MonoBehaviour
{
    [SerializeField]
    private FoodSpawner foodSpawner;

    public bool active;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (active)
        {
            collision.gameObject.SetActive(false);
            if (foodSpawner)
            {
                foodSpawner.spawnedFood.Remove(collision.gameObject);
            }
        }
    }
}
