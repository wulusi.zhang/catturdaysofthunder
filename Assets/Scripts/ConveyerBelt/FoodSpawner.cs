﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodSpawner : MonoBehaviour
{
    [SerializeField]
    private sObj_ItemSpawnerParams sObj_ItemParams;

    [SerializeField]
    private List<GameObject> foodList = new List<GameObject>();

    public List<GameObject> spawnedFood = new List<GameObject>();

    [SerializeField]
    private List<Transform> spawns = new List<Transform>();

    PoolManager poolManager;

    float duration;

    [SerializeField]
    private Animator beltAnimator;

    public Transform currentSpawn;

    // Start is called before the first frame update
    void Start()
    {
        poolManager = PoolManager.Instance;
        duration = sObj_ItemParams.spawnCoolDown;
        StartCoroutine(Countdown(duration));
        currentSpawn = spawns[0];
    }

    // Update is called once per frame
    void Update()
    {
        UpdateItemParams();
        UpdateColliders();
    }

    private IEnumerator Countdown(float duration)
    {
        float totalTime = 0;
        while (totalTime <= duration)
        {
            totalTime += Time.deltaTime;
            yield return null;
        }
        spawnFromPool(currentSpawn);
    }

    private void spawnFromPool(Transform spawnLoc)
    {
        GameObject spawnedItem = poolManager.SpawnFromPool(foodList[Random.Range(0, foodList.Count - 1)].name, spawnLoc.position, Quaternion.identity);

        spawnedFood.Add(spawnedItem);

        StartCoroutine(Countdown(duration));
    }

    private void UpdateItemParams()
    {
        for (int i = 0; i < spawnedFood.Count - 1; i++)
        {
            var currentSpd = sObj_ItemParams.speed;
            var currentDir = Mathf.Sign(sObj_ItemParams.direction);

            var lastSpd = spawnedFood[i].GetComponent<ItemMover>().itemSpeed;
            var lastDir = spawnedFood[i].GetComponent<ItemMover>().direction;

            if (lastSpd != currentSpd || lastDir != currentDir)
            {
                spawnedFood[i].GetComponent<ItemMover>().itemSpeed = currentSpd;
                //Mathf.Lerp(spawnedFood[i].GetComponent<ItemMover>().itemSpeed, currentSpd, Time.deltaTime * 0.5f);
                //Debug.Log("Lerping speed" + spawnedFood[i].GetComponent<ItemMover>().itemSpeed);

                spawnedFood[i].GetComponent<ItemMover>().direction =
                Mathf.Lerp(spawnedFood[i].GetComponent<ItemMover>().direction, Mathf.Sign(currentDir), Time.deltaTime * 0.5f);
                StartCoroutine(PauseSpawn(3f));
                //Debug.Log("Lerping dir" + spawnedFood[i].GetComponent<ItemMover>().itemSpeed);

                beltAnimator.SetFloat("direction", currentDir);
                beltAnimator.speed = currentSpd * 0.1f;
                duration = sObj_ItemParams.spawnCoolDown;
            }
            else
            {
                spawnedFood[i].GetComponent<ItemMover>().itemSpeed = sObj_ItemParams.speed;
                spawnedFood[i].GetComponent<ItemMover>().direction = Mathf.Sign(sObj_ItemParams.direction);
                beltAnimator.SetFloat("direction", currentDir);
                beltAnimator.speed = currentSpd * 0.1f;
                duration = sObj_ItemParams.spawnCoolDown;
            }
        }
    }

    private IEnumerator PauseSpawn(float currentDuration)
    {
        duration = 100f;
        float totalTime = 0;
        while (totalTime <= currentDuration)
        {
            totalTime += Time.deltaTime;
            yield return null;
        }
        duration = sObj_ItemParams.spawnCoolDown;
    }

    private void UpdateColliders()
    {
        if (Mathf.Sign(sObj_ItemParams.direction) > 0)
        {
            //Going right
            currentSpawn = spawns[1];
            spawns[0].GetComponent<Despawner>().active = true;
            spawns[1].GetComponent<Despawner>().active = false;
        }
        else
        {
            //Going left
            currentSpawn = spawns[0];
            spawns[0].GetComponent<Despawner>().active = false;
            spawns[1].GetComponent<Despawner>().active = true;
        }
    }
}
